terraform {
  backend "s3" {
    region = "us-east-1"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.58.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"

  default_tags {
    tags = {
      Environment = var.environment
      Project     = "nbazinet"
      CodeSource  = "gitlab.com/dialot-workshop/websites/nbazinet"
      Managed     = "Terraform"
    }
  }
}
