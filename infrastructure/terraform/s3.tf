resource "aws_s3_bucket" "nbazinet-static-website" {
  bucket = "${var.identifier}-${var.environment}-nbazinet-static-sites"
}

resource "aws_s3_bucket_public_access_block" "nbazinet-s3-block-public" {
  bucket = aws_s3_bucket.nbazinet-static-website.id
  block_public_acls = true
  block_public_policy = true
  ignore_public_acls = true
  restrict_public_buckets = true
}

data "aws_iam_policy_document" "cloudfront-s3-policy-document" {
  policy_id = "PolicyForCloudFrontPrivateContent"
  statement {
    sid = "1"
    actions = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.nbazinet-static-website.arn}/*"]
    principals {
      type = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.cf-access-identity.iam_arn]
    }
  }
}

resource "aws_s3_bucket_policy" "cloudfront-s3-bucket-policy" {
  bucket = aws_s3_bucket.nbazinet-static-website.id
  policy = data.aws_iam_policy_document.cloudfront-s3-policy-document.json
}

resource "aws_s3_bucket" "nbazinet-static-website-logs" {
  bucket = "${var.identifier}-${var.environment}-nbazinet-static-sites-cf-logs"
}

resource "aws_s3_bucket_public_access_block" "nbazinet-static-website-logs" {
  bucket = aws_s3_bucket.nbazinet-static-website-logs.id
  block_public_acls = true
  block_public_policy = true
  ignore_public_acls = true
  restrict_public_buckets = true
}