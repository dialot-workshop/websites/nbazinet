data "aws_caller_identity" "current" {}

data "aws_acm_certificate" "related_certs" {
    domain          = var.main_dns
    statuses        = ["ISSUED"]
    most_recent = true
}

data "aws_route53_zone" "hosted_zone" {
    name            = var.hosted_zone
    private_zone    = false
}