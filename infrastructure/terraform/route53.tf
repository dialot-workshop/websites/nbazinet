resource "aws_route53_record" "main" {
  zone_id   = data.aws_route53_zone.hosted_zone.zone_id
  name      = var.main_dns
  type      = "A"

  alias {
    name                    = "${aws_cloudfront_distribution.cf-distribution.domain_name}"
    zone_id                 = "${aws_cloudfront_distribution.cf-distribution.hosted_zone_id}"
    evaluate_target_health  = false
  }
}