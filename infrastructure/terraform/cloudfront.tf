
resource "aws_cloudfront_origin_access_identity" "cf-access-identity" {
  comment = "access-identity-${var.identifier}-${var.environment}-nbazinet-static-sites-us-east-1.s3.amazonaws.com"
}

locals {
  s3_origin_id = "${var.identifier}-${var.environment}-nbazinet-static-sites"
}

resource "aws_cloudfront_distribution" "cf-distribution" {

  origin {
    domain_name = aws_s3_bucket.nbazinet-static-website.bucket_regional_domain_name
    origin_id   = local.s3_origin_id

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.cf-access-identity.cloudfront_access_identity_path
    }
  }
  
  default_root_object = "index.html"
  enabled             = true
  is_ipv6_enabled     = true
  wait_for_deployment = false

  logging_config {
    include_cookies = false
    bucket          = aws_s3_bucket.nbazinet-static-website-logs.bucket_regional_domain_name
  }

  aliases = [var.main_dns]

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = local.s3_origin_id

    forwarded_values {
      query_string = true

      cookies {
        forward = "none"
      }
      query_string_cache_keys = ["d" ]
    }

    min_ttl                = 0
    default_ttl            = 86400
    max_ttl                = 86400
    compress               = true
    viewer_protocol_policy = "redirect-to-https"
  }

  price_class = "PriceClass_All"

  viewer_certificate {
    acm_certificate_arn       = data.aws_acm_certificate.related_certs.arn
    minimum_protocol_version  = "TLSv1.2_2021"
    ssl_support_method        = "sni-only"
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
}