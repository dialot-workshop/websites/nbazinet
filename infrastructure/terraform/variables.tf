variable "identifier" {
  description = "Unique identifier added on resource name"
  type = string
}

variable "environment" {
  description = "Environment this plan was ran"
  type = string
}

variable "hosted_zone" {
  description = "Name of the Related Hosted Zone."
  type        = string
}

variable "main_dns" {
  description = "Name of application Web Site"
  type        = string
}