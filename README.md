# nbazinet

This project is used to deploy a static website in order to promote the work of myself, Nicolas Bazinet.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app) and is using 
`emailjs-com` for sending Emails. 

# Local Installation

In order to start the application locally, you need react installed. Then you can run `npm start` command to start the
app, and access it through localhost:3000. 

The following commands can also be useful. 

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

# Cloud Infrastructure

Inside the `./infrastructure/terraform` folder, you can find the infrastructure created in order to make this application
run in AWS. This simple architecture can be represented like so:

![nbazinet](doc/nbazinet.png)

# Authors

- [Nicolas Bazinet](https://gitlab.com/nbazinet)

# Project Status

In Development.