module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      backgroundImage: {
        'rock-background': "url('../public/rock.jpg')",
      }
    },
  }
}
